import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader

import ast
import ujson
import pandas as pd

numerical_attrs = map(lambda x: x.strip(), open('./processed/num_features').readlines())
category_attrs = map(lambda x: x.strip(), open('./processed/cat_features').readlines())

def get_value_in_dict(x, k):
    return x[k]

class MySet(Dataset):
    def __init__(self, filenames):
        super(MySet, self).__init__()
        self.content = []
        for filename in filenames:
            self.content += open('./processed/{}'.format(filename)).readlines()

    def __len__(self):
        return len(self.content)

    def __getitem__(self, idx):
        return ujson.loads(self.content[idx])

def collate_fn(recs, max_df):
    data = {}

    for attr in numerical_attrs:
        value = torch.FloatTensor(map(lambda x: get_value_in_dict(x, attr), recs))
        data[attr] = {'value': value, 'type': 'numerical'}

    for attr in category_attrs:
        value = torch.FloatTensor(map(lambda x: get_value_in_dict(x, attr), recs)).long()
        data[attr] = {'value': value, 'type': 'category', 'max': max_df.loc[attr]['max']}

    item_property_list = torch.FloatTensor(map(lambda x: get_value_in_dict(x, 'item_property_list'), recs)).long()
    item_property_mask = torch.FloatTensor(map(lambda x: get_value_in_dict(x, 'item_property_mask'), recs))
    data['item_property_list'] = {'value': item_property_list, 'mask': item_property_mask, 'type': 'list', 'max': 56000, 'length': item_property_list.size()[1]}

    predict_category_list = torch.FloatTensor(map(lambda x: get_value_in_dict(x, 'predict_category_list'), recs)).long()
    predict_category_mask = torch.FloatTensor(map(lambda x: get_value_in_dict(x, 'predict_category_mask'), recs))
    data['predict_category_list'] = {'value': predict_category_list, 'mask': predict_category_mask, 'type': 'list', 'max': 800, 'length': predict_category_list.size()[1]}

    predict_property_list = torch.FloatTensor(map(lambda x: get_value_in_dict(x, 'predict_property_list'), recs)).long()
    predict_property_mask = torch.FloatTensor(map(lambda x: get_value_in_dict(x, 'predict_property_mask'), recs))
    data['predict_property_list'] = {'value': predict_property_list, 'mask': predict_property_mask, 'type': 'list', 'max': 56000, 'length': predict_property_list.size()[1]}

    data['instance_id'] = map(lambda x: get_value_in_dict(x, 'instance_id'), recs)

    return data

def get_loader(filenames, batch_size = 64, shuffle = True):
    dataset = MySet(filenames)

    max_df = pd.read_csv('./processed/max_df.csv').set_index('attr')

    data_iter = DataLoader(dataset = dataset, \
                           batch_size = batch_size, \
                           num_workers = 4, \
                           shuffle = shuffle, \
                           pin_memory = True, \
                           collate_fn = lambda x: collate_fn(x, max_df)
    )

    return data_iter

if __name__ == '__main__':
    data_iter = get_loader(['train_part_%d' % i for i in range(5)])
    for idx, item in enumerate(data_iter):
        print idx


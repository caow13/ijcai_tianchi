import torch

from torch.autograd import Variable

def to_var(var):
    if torch.is_tensor(var):
        var = Variable(var)
        if torch.cuda.is_available():
            var = var.cuda()
        return var
    elif isinstance(var, dict):
        for key in var:
            var[key] = to_var(var[key])
        return var
    elif isinstance(var, list):
        var = map(lambda x: to_var(x), var)
        return var
    else:
        return var


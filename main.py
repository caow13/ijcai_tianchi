import argparse

import torch
import torch.nn as nn
import torch.optim as optim

from torch.optim.lr_scheduler import StepLR

import utils
import models
import data_loader

import pandas as pd

parser = argparse.ArgumentParser()
parser.add_argument('--epochs', type = int, default = 30)
parser.add_argument('--batch_size', type = int, default = 64)
parser.add_argument('--fold', type = int)
parser.add_argument('--model', type = str)
parser.add_argument('--opt', type = str)
parser.add_argument('--weight_decay', type = float, default = 1e-3)
parser.add_argument('--decay_step', type = int, default = 5)
parser.add_argument('--save_file', type = str)
args = parser.parse_args()

all_files = map(lambda x: 'train_part_{}'.format(x), range(100))

train_files = ['train_part_%d' % i for i in range(5)]
train_files.remove('train_part_%d' % args.fold)

train_iter = data_loader.get_loader(train_files, shuffle = True, batch_size = args.batch_size)
val_files = ['train_part_%d' % args.fold]
test_files = ['test']

val_iter = data_loader.get_loader(val_files, shuffle = False, batch_size = args.batch_size)
test_iter = data_loader.get_loader(test_files, shuffle = False, batch_size = args.batch_size)

def save_to_file(pred, id_, output_file):
    data = pd.DataFrame({'predicted_score': pred, 'instance_id': id_})
    data.to_csv(output_file)

def train(model):
    sgd_optimizer = optim.SGD(model.parameters(), lr = 1e-3, momentum = 0.9, weight_decay = args.weight_decay, nesterov = True)
    sgd_scheduler = StepLR(sgd_optimizer, step_size = args.decay_step, gamma = 0.5)

    adam_optimizer = optim.Adam(model.parameters(), lr = 1e-3, weight_decay = args.weight_decay)
    adam_scheduler = StepLR(adam_optimizer, step_size = args.decay_step, gamma = 0.5)

    if args.opt == 'sgd':
        optimizer = sgd_optimizer
        scheduler = sgd_scheduler
    elif args.opt == 'adam':
        optimizer = adam_optimizer
        scheduler = adam_scheduler

    best_val_loss = 1e30

    for epoch in range(args.epochs):
        model.train()

        sgd_scheduler.step()

        running_loss = 0.0

        for idx, data in enumerate(train_iter):
            data = utils.to_var(data)
            ret = model.run_on_batch(data, optimizer = optimizer)

            running_loss += ret['loss'].data[0]

            if idx % 10 == 0:
                print '\r Progress epoch {}, {:.5f}, average loss {}'.format(epoch, (idx + 1) * 100.0 / len(train_iter), running_loss / (idx + 1.0)),

        ret = test(model, val_iter)
        print 'Eval loss {:.5f}'.format(ret['loss'])

        if ret['loss'] < best_val_loss:
            best_val_loss = ret['loss']

            ret = test(model, test_iter)
            save_to_file(ret['pred'], ret['id'], './saves/{}/{}.txt'.format(args.model, args.fold))

def test(model, data_iter):
    model.eval()

    running_loss = 0.0

    pred = []
    id_ = []

    for idx, data in enumerate(data_iter):
        data = utils.to_var(data)
        ret = model.run_on_batch(data, optimizer = None)
        running_loss += ret['loss'].data[0]
        pred += ret['pred'].view(-1).data.tolist()
        id_ += data['instance_id']

    return {'loss': running_loss / len(data_iter), 'pred': pred, 'id': id_}

def run():
    model = getattr(models, args.model).Model()
    model.build(val_iter.__iter__().next())

    if torch.cuda.is_available():
        model = model.cuda()

    train(model)

if __name__ == '__main__':
    run()

import argparse
import itertools

import numpy as np
import pandas as pd

from collections import Counter

from ipdb import set_trace

EPS = 1e-5


df_train = pd.read_csv('./raw/round1_ijcai_18_train_20180301.txt', sep = ' ')
df_test_a = pd.read_csv('./raw/round1_ijcai_18_test_a_20180301.txt', sep = ' ')
df_test_b = pd.read_csv('./raw/round1_ijcai_18_test_b_20180418.txt', sep = ' ')

df = pd.concat([df_train, df_test_a, df_test_b], axis = 0)
df = df.drop_duplicates()
df = df.replace(-1, np.nan)

def category_id_map(cats):
    cats_d = cats.unique()
    cats_d = dict(zip(cats_d, range(len(cats_d))))
    # leave 0 for the default value
    return cats.map(lambda x: cats_d.get(x, -1) + 1)

def get_category_dict(df):
    '''
    Return dict: category to int
    '''
    def collect_category(rec):
        rec = rec.split(';')
        return map(lambda x: x.split(':')[0], rec)

    item_category_list = df['item_category_list'].map(lambda x: x.split(';')[1:]).tolist()
    item_category_list = itertools.chain(*item_category_list)

    predict_category_list = df['predict_category_property'].map(lambda x: collect_category(x)).tolist()
    predict_category_list = itertools.chain(*predict_category_list)

    category_counter = Counter(item_category_list)
    category_counter.update(Counter(predict_category_list))

    del category_counter['-1']

    category_list = filter(lambda x: x[1] >= 2, category_counter.items())
    category_list = map(lambda x: x[0], category_list)

    category_d = zip(category_list, range(len(category_list)))
    category_d = dict(category_d)

    print 'Total categories {}'.format(len(category_d))

    return category_d

def get_property_dict(df):
    '''
    Return dict: property to dict
    '''
    def collect_property(rec):
        rec = rec.split(';')
        if len(rec) <= 1:
            return []
        else:
            property_ = map(lambda x: x.split(':')[1].split(','), rec)
            return list(itertools.chain(*property_))

    item_property_list = df['item_property_list'].map(lambda x: x.split(';')).tolist()
    item_property_list = itertools.chain(*item_property_list)

    predict_property_list = df['predict_category_property'].map(lambda x: collect_property(x)).tolist()
    predict_property_list = itertools.chain(*predict_property_list)

    property_counter = Counter(item_property_list)
    property_counter.update(Counter(predict_property_list))

    del property_counter['-1']

    property_list = filter(lambda x: x[1] >= 2, property_counter.items())
    property_list = map(lambda x: x[0], property_list)

    property_d = zip(property_list, range(len(property_list)))
    property_d = dict(property_d)

    print 'Total properties {}'.format(len(property_d))

    return property_d

def standard_norm(vals):
    return (vals - vals.mean()) / vals.std()

def parse_time(df):
    time_ = pd.to_datetime(df['context_timestamp'], unit = 's')

    df['time'] = time_
    df['day'] = time_.map(lambda x: x.day)
    df['hour'] = time_.map(lambda x: x.hour)

    return df

def parse_item(df):
    def parse_category_list(rec, max_len):
        rec = rec.split(';')[1:]

        category = [0] * max_len
        mask = [0] * max_len

        for i, cat in enumerate(rec):
            if cate_dict.has_key(cat):
                category[i] = cate_dict[cat]
                mask[i] = 1

        return category, mask

    def parse_property_list(rec, max_len):
        rec = rec.split(';')

        property_ = [0] * max_len
        mask = [0] * max_len

        for i, prop in enumerate(rec):
            if prop_dict.has_key(prop):
                property_[i] = prop_dict[prop]
                mask[i] = 1

        return property_, mask

    ######### category ###########
    category_mask = df['item_category_list'].map(lambda x: parse_category_list(x, max_len = 2))
    category = map(lambda x: x[0], category_mask)
    mask = map(lambda x: x[1], category_mask)

    df['item_category_list'] = category
    df['item_category_mask'] = mask

    ######### property ###########
    property_mask = df['item_property_list'].map(lambda x: parse_property_list(x, max_len = 100))
    property_ = map(lambda x: x[0], property_mask)
    mask = map(lambda x: x[1], property_mask)

    df['item_property_list'] = property_
    df['item_property_mask'] = mask

    ######## other attributes ######
    df['item_id'] = category_id_map(df['item_id'])
    df['item_brand_id'] = category_id_map(df['item_brand_id'])
    df['item_city_id'] = category_id_map(df['item_city_id'])

    df['item_price_level'] = standard_norm(df['item_price_level'])
    df['item_sales_level'] = standard_norm(df['item_sales_level'])
    df['item_collected_level'] = standard_norm(df['item_collected_level'])
    df['item_pv_level'] = standard_norm(df['item_pv_level'])

    df['item_price_level_cat'] = category_id_map(df['item_price_level'])
    df['item_sales_level_cat'] = category_id_map(df['item_sales_level'])
    df['item_collected_level_cat'] = category_id_map(df['item_collected_level'])
    df['item_pv_level_cat'] = category_id_map(df['item_pv_level'])

    return df

def parse_user(df):
    df['user_id'] = category_id_map(df['user_id'])
    df['user_gender_id'] = category_id_map(df['user_gender_id'])
    df['user_occupation_id'] = category_id_map(df['user_occupation_id'])

    df['user_age_level'] = standard_norm(df['user_age_level'])
    df['user_star_level'] = standard_norm(df['user_star_level'])

    df['user_age_level_cat'] = category_id_map(df['user_age_level'])
    df['user_star_level_cat'] = category_id_map(df['user_star_level'])

    return df

def parse_context(df):
    def parse_category_list(rec, max_len):
        rec = map(lambda x: x.split(':')[0], rec.split(';'))

        category = [0] * max_len
        mask = [0] * max_len

        for i, cat in enumerate(rec):
            if cate_dict.has_key(cat):
                category[i] = cate_dict[cat]
                mask[i] = 1

        return category, mask

    def parse_property_list(rec, max_len):
        rec = rec.split(';')
        if len(rec) <= 1:
            rec = []
        else:
            rec = map(lambda x: x.split(':')[1].split(','), rec)
            rec = list(itertools.chain(*rec))

        property_ = [0] * max_len
        mask = [0] * max_len

        for i, prop in enumerate(rec):
            if prop_dict.has_key(prop):
                property_[i] = prop_dict[prop]
                mask[i] = 1

        return property_, mask

    # regard page id as a level attribute
    df['context_page_level'] = standard_norm(df['context_page_id'])
    df['context_page_level_cat'] = category_id_map(df['context_page_id'])

    ######## category ##############
    category_mask = df['predict_category_property'].map(lambda x: parse_category_list(x, max_len = 14))
    category = map(lambda x: x[0], category_mask)
    mask = map(lambda x: x[1], category_mask)

    df['predict_category_list']= category
    df['predict_category_mask'] = mask

    ######## property ##############
    property_mask = df['predict_category_property'].map(lambda x: parse_property_list(x, max_len = 17))
    property_ = map(lambda x: x[0], property_mask)
    mask = map(lambda x: x[1], property_mask)

    df['predict_property_list'] = property_
    df['predict_property_mask'] = mask

    return df

def parse_shop(df):
    df['shop_id'] = category_id_map(df['shop_id'])
    df['shop_review_num_level_cat'] = category_id_map(df['shop_review_num_level'])
    df['shop_star_level_cat']= category_id_map(df['shop_star_level'])

    df['shop_review_num_level'] = standard_norm(df['shop_review_num_level'])
    df['shop_review_positive_rate'] = standard_norm(df['shop_review_positive_rate'])
    df['shop_star_level']= standard_norm(df['shop_star_level'])
    df['shop_score_service'] = standard_norm(df['shop_score_service'])
    df['shop_score_delivery'] = standard_norm(df['shop_score_delivery'])
    df['shop_score_description'] = standard_norm(df['shop_score_description'])

    return df

def parse_avg(df):
    def parse(cur_df, attr, end_day):
        prev_df = df[df['day'] < end_day]

        feature = '_'.join(attr)

        day_gap = end_day - 18

        click = prev_df.groupby(attr).size().reset_index().rename(columns = {0: 'click_{}'.format(feature)})
        click['click_{}'.format(feature)] /= day_gap

        cur_df = pd.merge(cur_df, click, how = 'left', on = attr)

        click_trade = prev_df[prev_df['is_trade'] == 1].groupby(attr).size().reset_index().rename(columns = {0: 'click_trade_{}'.format(feature)})
        click_trade['click_trade_{}'.format(feature)] /= day_gap

        cur_df = pd.merge(cur_df, click_trade, how = 'left', on = attr)

        cur_df['click_{}'.format(feature)] = cur_df['click_{}'.format(feature)].fillna(0.0)
        cur_df['click_trade_{}'.format(feature)] = cur_df['click_trade_{}'.format(feature)].fillna(0.0)

        cur_df['click_trade_ratio_{}'.format(feature)] = (cur_df['click_trade_{}'.format(feature)] + 1.0) / (cur_df['click_{}'.format(feature)] + 10.0)

        return cur_df


    def run(attrs):
        rets = []

        for day in range(19, 25 + 1):
            cur_df = df[df['day'] == day]
            for attr in attrs:
                print 'avg', day, attr
                if isinstance(attr, str):
                    attr = [attr]
                elif isinstance(attr, tuple):
                    attr = list(attr)

                cur_df = parse(cur_df, attr, day)
            rets.append(cur_df)

        cur_df = pd.concat(rets)

        for attr in attrs:
            if isinstance(attr, str):
                attr = [attr]
            elif isinstance(attr, tuple):
                attr = list(attr)

            feature = '_'.join(attr)
            cur_df['click_{}'.format(feature)] = standard_norm(cur_df['click_{}'.format(feature)])
            cur_df['click_trade_{}'.format(feature)] = standard_norm(cur_df['click_trade_{}'.format(feature)])
            cur_df['click_trade_ratio_{}'.format(feature)] = standard_norm(cur_df['click_trade_ratio_{}'.format(feature)])

        return cur_df

    attrs = ['item_id', 'hour', 'item_brand_id', 'item_city_id', 'item_price_level_cat', 'item_sales_level_cat', 'item_collected_level_cat', 'item_pv_level_cat', 'user_id', 'user_gender_id','user_age_level_cat', 'user_occupation_id', 'user_star_level_cat', 'context_page_id','shop_id', 'shop_review_num_level_cat', 'shop_star_level_cat']

#    user_combs = itertools.combinations(['user_id', 'user_gender_id', 'user_age_level_cat', 'user_occupation_id', 'user_star_level_cat','hour'], 2)
#    item_combs = itertools.combinations(['item_id', 'item_brand_id', 'item_city_id', 'item_price_level_cat', 'item_sales_level_cat', 'item_collected_level_cat', 'item_pv_level_cat','hour'], 2)
#    shop_combs = itertools.combinations(['shop_id', 'shop_review_num_level_cat', 'shop_star_level_cat', 'hour'], 2)
#    cross_combs = itertools.combinations(['item_brand_id', 'hour', 'item_brand_id', 'item_price_level_cat', 'item_sales_level_cat', 'user_id', 'shop_id', 'shop_star_level_cat'], 2)

    cross_combs = itertools.combinations(['user_id', 'item_id', 'shop_id'], 2)
    attrs = set(attrs + list(cross_combs))

    df = run(attrs)

    return df

def parse_time_leak(df):
    def parse(cur_df, attr):
        feature = attr

        first = cur_df[[attr, 'context_timestamp']].groupby(attr).min()
        first = first.reset_index()
        first.columns = [attr, '{}_first'.format(feature)]

        last = cur_df[[attr, 'context_timestamp']].groupby(attr).max()
        last = last.reset_index()
        last.columns = [attr, '{}_last'.format(feature)]

        cur_df = pd.merge(cur_df, first, on = attr, how = 'left')
        cur_df = pd.merge(cur_df, last, on = attr, how = 'left')

        cur_df['{}_gap'.format(feature)] = cur_df['{}_last'.format(feature)] - cur_df['{}_first'.format(feature)]

        return cur_df

    attrs = ['item_id', 'hour', 'item_brand_id', 'item_city_id', 'item_price_level_cat', 'item_sales_level_cat', 'item_collected_level_cat', 'item_pv_level_cat', 'user_id', 'user_gender_id','user_age_level_cat', 'user_occupation_id', 'user_star_level_cat', 'context_page_id','shop_id', 'shop_review_num_level_cat', 'shop_star_level_cat']

    rets = []
    for day in range(19, 25 + 1):
        cur_df = df[df['day'] == day]
        for attr in attrs:
            print 'time', day, attr
            cur_df = parse(cur_df, attr)
        rets.append(cur_df)

    cur_df = pd.concat(rets)


    for attr in attrs:
        feature = attr
        cur_df['{}_first'.format(feature)] = standard_norm(cur_df['{}_first'.format(feature)])
        cur_df['{}_last'.format(feature)] = standard_norm(cur_df['{}_last'.format(feature)])
        cur_df['{}_gap'.format(feature)] = standard_norm(cur_df['{}_gap'.format(feature)])

    return cur_df


def parse_rank_leak(df):
    def parse(cur_df, attr):
        feature = '_'.join(attr)

        click = cur_df.groupby(attr).size().reset_index().rename(columns = {0: '{}_daily_click'.format(feature)})
        cur_df = pd.merge(cur_df, click, how = 'left', on = attr)

        cur_df['{}_daily_rank'.format(feature)] = cur_df.groupby(attr)['context_timestamp'].rank(method = 'min').reset_index(drop = True)

        return cur_df

    attrs = ['item_id', 'hour', 'item_brand_id', 'item_city_id', 'item_price_level_cat', 'item_sales_level_cat', 'item_collected_level_cat', 'item_pv_level_cat', 'user_id', 'user_gender_id','user_age_level_cat', 'user_occupation_id', 'user_star_level_cat', 'context_page_id','shop_id', 'shop_review_num_level_cat', 'shop_star_level_cat']

#    user_combs = itertools.combinations(['user_id', 'user_gender_id', 'user_age_level_cat', 'user_occupation_id', 'user_star_level_cat','hour'], 2)
#    item_combs = itertools.combinations(['item_id', 'item_brand_id', 'item_city_id', 'item_price_level_cat', 'item_sales_level_cat', 'item_collected_level_cat', 'item_pv_level_cat','hour'], 2)
#    shop_combs = itertools.combinations(['shop_id', 'shop_review_num_level_cat', 'shop_star_level_cat', 'hour'], 2)
#    cross_combs = itertools.combinations(['item_id', 'hour', 'item_brand_id', 'item_price_level_cat', 'item_sales_level_cat', 'user_id', 'shop_id', 'shop_star_level_cat'], 2)
#
#    attrs = set(attrs + list(user_combs) + list(item_combs) + list(shop_combs) + list(cross_combs))

    cross_combs = itertools.combinations(['user_id', 'item_id', 'shop_id'], 2)
    attrs = set(attrs + list(cross_combs))

    rets = []
    for day in range(19, 25 + 1):
        cur_df = df[df['day'] == day]
        for attr in attrs:
            print 'rank', day, attr
            if isinstance(attr, str):
                attr = [attr]
            elif isinstance(attr, tuple):
                attr = list(attr)
            cur_df = parse(cur_df, attr)
        rets.append(cur_df)

    cur_df = pd.concat(rets)

    for attr in attrs:
        if isinstance(attr, str):
            attr = [attr]
        elif isinstance(attr, tuple):
            attr = list(attr)

        feature = '_'.join(attr)
        cur_df['{}_daily_click'.format(feature)] = standard_norm(cur_df['{}_daily_click'.format(feature)])
        cur_df['{}_daily_rank'.format(feature)] = standard_norm(cur_df['{}_daily_rank'.format(feature)])

    return cur_df


################# GET FREQUENT CATEGORY/PROPERTY #################
cate_dict = get_category_dict(df)
prop_dict = get_property_dict(df)
################# PARSE INPUT ###################
df = parse_time(df)
print 'Time Done'
df = parse_item(df)
print 'Item Done'
df = parse_user(df)
print 'User Done'
print df['user_id']
df = parse_context(df)
print 'Context Done'
print df
df = parse_shop(df)
print 'Shop Done'
############### PARSE AVG ###################
df = parse_avg(df)
############### PARSE AVG ###################
df = parse_time_leak(df)
df = parse_rank_leak(df)


df.to_csv('./processed/processed.csv')

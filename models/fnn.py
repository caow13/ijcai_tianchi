import torch
import torch.nn as nn
import torch.nn.functional as F

import numpy as np

EM_DIM = 16

class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()

    def build(self, data):
        input_dim = 0

        for attr in data:
            if attr == 'is_trade' or attr == 'instance_id': continue

            content = data[attr]

            assert(content['type'] in ['numerical', 'category', 'list'])

            if content['type'] == 'numerical':
                input_dim += 1
            else:
                setattr(self, 'em_{}'.format(attr), nn.Embedding(content['max'] + 1, EM_DIM, sparse = False))
                input_dim += EM_DIM

#        self.hid1 = nn.Linear(input_dim, 128)
#        self.hid2 = nn.Linear(128, 64)
        self.out = nn.Linear(input_dim, 1)

    def forward(self, data):
        inputs = []

        for attr in data:
            if attr == 'is_trade' or attr == 'instance_id': continue

            content = data[attr]

            assert(content['type'] in ['numerical', 'category', 'list'])

            if content['type'] == 'numerical':
                inputs.append(content['value'].view(-1, 1))
            elif content['type'] == 'category':
                value = content['value'].view(-1, 1)
                inputs.append(getattr(self, 'em_{}'.format(attr))(value).squeeze(dim = 1))
            else:
                value = getattr(self, 'em_{}'.format(attr))(content['value'])
                mask = content['mask'].unsqueeze(dim = 1)
                value = torch.bmm(mask, value).squeeze(dim = 1)
                inputs.append(value)

        inputs = torch.cat(inputs, dim = 1)

#        hid = F.leaky_relu(self.hid1(inputs))
#        hid = F.leaky_relu(self.hid2(hid))
        out = F.sigmoid(self.out(inputs))

        return out

    def run_on_batch(self, data, optimizer = None):
        pred = self(data)
        label = data['is_trade']['value'].view(-1, 1)
        loss = F.binary_cross_entropy(pred, label)

        def optim_step():
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        if optimizer is not None:
            optim_step()

        return {'pred': pred, 'label': label, 'loss': loss}

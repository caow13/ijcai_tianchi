import torch
import torch.nn as nn
import torch.nn.functional as F

import math
import numpy as np

EM_DIM = 16
NUM_CATS = open('./processed/cat_features').readlines().__len__() + 3
PROD_DIM = 48

class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()

    def build(self, data):
        numerical_dim = 0
        category_dim = 0

        for attr in data:
            if attr == 'is_trade' or attr == 'instance_id': continue

            content = data[attr]

            assert(content['type'] in ['numerical', 'category', 'list'])

            if content['type'] == 'numerical':
                shape = content['value'].size()
                numerical_dim += 1 if len(shape) == 1 else shape[1]
            else:
                setattr(self, 'em_{}'.format(attr), nn.Embedding(content['max'] + 1, EM_DIM, sparse = False))
                category_dim += EM_DIM

        # 21: number of category features
        self.weight_z = nn.Parameter(torch.Tensor(PROD_DIM, NUM_CATS, EM_DIM))
        # weight for inner production
        self.weight_pi = nn.Parameter(torch.Tensor(PROD_DIM, NUM_CATS, NUM_CATS))

        ########### output layer #############
        self.wide_and_deep_product = nn.Linear(PROD_DIM * 2 + numerical_dim + category_dim, 128)
        self.out = nn.Linear(128, 1)

        ########## reset the weight of product layer parameters ##########
        self.reset_parameters(self.weight_z)
        self.reset_parameters(self.weight_pi, gain = 1. / math.sqrt(EM_DIM))

    def reset_parameters(self, weight, gain = 1.0):
        stdv = 1. / math.sqrt(weight.size(1) * weight.size(2))
        weight.data.uniform_(-stdv, stdv)
        weight.data.mul_(gain)

    def calc_layer_z(self, category_inputs):
        category_inputs = category_inputs.unsqueeze(dim = 1)
        weight_z = self.weight_z.unsqueeze(dim = 0)

        layer_z = torch.mul(category_inputs, weight_z)
        layer_z = torch.sum(layer_z, dim = -1)
        layer_z = torch.sum(layer_z, dim = -1)

        return layer_z

    def calc_layer_pi(self, category_inputs):
        category_inputs_perm = category_inputs.permute(0, 2, 1)
        category_inner_prod = torch.bmm(category_inputs, category_inputs_perm).unsqueeze(dim = 1)

        weight_pi = self.weight_pi.unsqueeze(dim = 0)

        layer_pi = torch.mul(category_inner_prod, weight_pi)
        layer_pi = torch.sum(layer_pi, dim = -1)
        layer_pi = torch.sum(layer_pi, dim = -1)

        return layer_pi

    def forward(self, data):
        numerical_inputs = []
        category_inputs = []

        for attr in data:
            if attr == 'is_trade' or attr == 'instance_id': continue

            content = data[attr]

            assert(content['type'] in ['numerical', 'category', 'list'])

            if content['type'] == 'numerical':
                if len(content['value'].size()) == 1:
                    numerical_inputs.append(content['value'].view(-1, 1))
                else:
                    numerical_inputs.append(content['value'])
            elif content['type'] == 'category':
                value = content['value'].view(-1, 1)
                category_inputs.append(getattr(self, 'em_{}'.format(attr))(value))
            else:
                value = getattr(self, 'em_{}'.format(attr))(content['value'])
                mask = content['mask'].unsqueeze(dim = 1)
                value = torch.bmm(mask, value)
                category_inputs.append(value)

        category_inputs = torch.cat(category_inputs, dim = 1)
        numerical_inputs = torch.cat(numerical_inputs, dim = 1)

        l_z = self.calc_layer_z(category_inputs)
        l_pi = self.calc_layer_pi(category_inputs)

        category_inputs = category_inputs.view(-1, NUM_CATS * EM_DIM)

        inputs = torch.cat([l_pi, l_z, numerical_inputs, category_inputs], dim = 1)

        hid = F.leaky_relu(self.wide_and_deep_product(inputs))

        out = F.sigmoid(self.out(hid))

        return out

    def run_on_batch(self, data, optimizer = None):
        pred = self(data)
        label = data['is_trade']['value'].view(-1, 1)
        loss = F.binary_cross_entropy(pred, label)

        def optim_step():
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        if optimizer is not None:
            optim_step()

        return {'pred': pred, 'label': label, 'loss': loss}

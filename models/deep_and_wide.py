import torch
import torch.nn as nn
import torch.nn.functional as F

from ipdb import set_trace

import numpy as np

EM_DIM = 16

numerical_attrs = map(lambda x: x.strip(), open('./processed/num_features').readlines())

class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()

    def build(self, data):
        input_dim = 0

        for attr in data:
            if attr == 'is_trade' or attr == 'instance_id': continue

            content = data[attr]

            assert(content['type'] in ['numerical', 'category', 'list'])

            if content['type'] == 'numerical':
                shape = content['value'].size()
                input_dim += 1 if len(shape) == 1 else shape[1]
            else:
                setattr(self, 'em_{}'.format(attr), nn.Embedding(content['max'] + 1, EM_DIM, sparse = False))
                input_dim += EM_DIM

        self.wide_out = nn.Linear(input_dim, 1)

        self.input2hid = nn.Linear(input_dim, 32)
        self.hid2hid = nn.ModuleList()

        for i in range(3):
            self.hid2hid.append(nn.Linear(32, 32))

        self.deep_out = nn.Linear(32, 1)

        self.comb_out = nn.Linear(2, 1)

    def forward(self, data):
        inputs = []

        for attr in data:
            if attr == 'is_trade' or attr == 'instance_id': continue

            content = data[attr]

            assert(content['type'] in ['numerical', 'category', 'list'])

            if content['type'] == 'numerical':
                if len(content['value'].size()) == 1:
                    inputs.append(content['value'].view(-1, 1))
                else:
                    inputs.append(content['value'])
            elif content['type'] == 'category':
                value = content['value'].view(-1, 1)
                inputs.append(getattr(self, 'em_{}'.format(attr))(value).squeeze(dim = 1))
            else:
                value = getattr(self, 'em_{}'.format(attr))(content['value'])
                mask = content['mask'].unsqueeze(dim = 1)
                value = torch.bmm(mask, value).squeeze(dim = 1)
                inputs.append(value)

        inputs = torch.cat(inputs, dim = 1)

        wide_out = F.sigmoid(self.wide_out(inputs))

        hid = F.leaky_relu(self.input2hid(inputs))
        for i in range(len(self.hid2hid)):
            hid = F.leaky_relu(self.hid2hid[i](hid))
        deep_out = F.sigmoid(self.deep_out(hid))

        comb = F.sigmoid(self.comb_out(torch.cat([wide_out, deep_out], dim = 1)))

        out = wide_out * comb + deep_out * (1 - comb)

        return out

    def run_on_batch(self, data, optimizer = None):
        pred = self(data)
        label = data['is_trade']['value'].view(-1, 1)
        loss = F.binary_cross_entropy(pred, label)

        def optim_step():
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        if optimizer is not None:
            optim_step()

        return {'pred': pred, 'label': label, 'loss': loss}

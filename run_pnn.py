import os
import sys

VAL_DAYS = range(0, 5)

for val_day in VAL_DAYS:
    print 'Run on K-fold on day {}'.format(val_day)
    print 'Start training'
    os.system('python main.py --model pnn --epochs 30 --fold {} --batch_size 64 --opt sgd --weight_decay 0.001'.format(val_day))

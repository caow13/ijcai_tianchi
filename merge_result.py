import os
import pandas as pd

df_list = []

df_test = pd.read_csv('./raw/round1_ijcai_18_test_b_20180418.txt', sep = ' ')
ids_ = df_test['instance_id'].tolist()

for file_ in os.listdir('./saves/deep_and_wide'):
    print 'deep and wide {}'.format(file_)
    df = pd.read_csv('./saves/deep_and_wide/{}'.format(file_))
    print df['predicted_score'].mean()
    df = df.drop(df.columns[0], axis = 1)
    df = df.set_index('instance_id')
    df = df.loc[ids_]
    df_list.append(df)


for file_ in os.listdir('./saves/pnn'):
    print 'pnn {}'.format(file_)
    df = pd.read_csv('./saves/deep_and_wide/{}'.format(file_))
    print df['predicted_score'].mean()
    df = df.drop(df.columns[0], axis = 1)
    df = df.set_index('instance_id')
    df = df.loc[ids_]
    df_list.append(df)


df_merge = reduce(lambda x, y: x + y, df_list)
df_merge = df_merge / len(df_list)
df_merge = df_merge.reset_index()

print df_merge.mean()

fs = open('./test_b.txt', 'w')
fs.write('instance_id predicted_score\r')
for i in range(df_merge.shape[0]):
    id_ = df_merge['instance_id'][i]
    score = df_merge['predicted_score'][i]
    fs.write('{} {}\r'.format(id_, score))
fs.close()
print 'end'

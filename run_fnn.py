import os
import sys

VAL_DAYS = range(17, 24 + 1)[::-1]

for val_day in VAL_DAYS:
    print 'Run on K-fold on day {}'.format(val_day)
    print 'Generating data'
    os.system('python get_processed_df.py --val_day {} --model fnn '.format(val_day))
    print 'Start training'
    os.system('mkdir -p ./saves/fnn')
    os.system('python main.py --model fnn --epochs 20 --val_day {} --batch_size 256 --weight_decay 0.0001 --opt adam --save_file weight'.format(val_day))
    print 'Collect result'
